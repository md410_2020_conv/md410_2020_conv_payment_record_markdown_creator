""" Create a payment record for the Lions MD410 2020 Convention
for a given registration number
"""

import attr
from decimal import Decimal, getcontext
import json
import os.path

from md410_2020_conv_common.db import DB as common_db
import sqlalchemy as sa

getcontext().prec = 20
TWOPLACES = Decimal(10) ** -2

TABLES = {
    "registree": ("md410_2020_conv", "registree"),
    "registree_pair": ("md410_2020_conv", "registree_pair"),
    "full_reg": ("md410_2020_conv", "full_reg"),
    "partial_reg": ("md410_2020_conv", "partial_reg"),
    "pins": ("md410_2020_conv", "pins"),
    "payment": ("md410_2020_conv", "payment")
}

COSTS = {
    "full": 1285,
    "banquet": 500,
    "convention": 400,
    "theme": 450,
    "pins": 55
}

@attr.s
class Payment(object):
    timestamp = attr.ib()
    amount = attr.ib()

@attr.s
class Registree(object):
    reg_num = attr.ib()
    first_names = attr.ib()
    last_name = attr.ib()
    initial_owed = attr.ib()
    payments = attr.ib()
    title = attr.ib(default=None)

    def __attrs_post_init__(self):
        if self.title:
            t = f"{self.title} "
        else:
            t = ""
        self.name = f"{t}{self.first_names.strip()} {self.last_name.strip()}"
        self.owed = 0

    def render(self):
        out = []
        out.append(f"### {self.name} (MDC{self.reg_num:03}) {{-}}")
        out.append(f"")
        out.append(f"|Details |Amount |")
        out.append(f"|:-|-:|")
        out.append(f"|**Initial Balance** | R{self.initial_owed}|")
        self.owed = self.initial_owed
        for p in self.payments:
            out.append(f"|Payment recorded on {p.timestamp:%y/%m/%d %H:%M} | R{p.amount}|")
            self.owed -= p.amount
        out.append(f"|**Total Owed** | R{self.owed}|")
        out.append(f"")
        return out


@attr.s
class DB(common_db):
    def set_reg_nums(self, reg_num):
        tp = self.tables["registree_pair"]
        res = self.engine.execute(sa.select([tp.c.first_reg_num, tp.c.second_reg_num],
                                            sa.or_(tp.c.first_reg_num == reg_num,
                                                   tp.c.second_reg_num == reg_num))).fetchone()
        if res:
            self.reg_nums = [res[0], res[1]]
        else:
            self.reg_nums = [reg_num]

    def get_registrees(self, reg_num):
        self.set_reg_nums(reg_num)
        tr = self.tables["registree"]
        tfr = self.tables["full_reg"]
        tpr = self.tables["partial_reg"]
        tp = self.tables["pins"]
        tpy = self.tables["payment"]

        registrees = []
        for rn in self.reg_nums:
            if rn:
                costs = {
                    "full": 0,
                    "banquet": 0,
                    "convention": 0,
                    "theme": 0,
                    "pins": 0
                }

                db_data = self.engine.execute(
                    tr.select(whereclause=tr.c.reg_num == rn)
                ).fetchone()
                data = {"reg_num": rn}
                if db_data:
                    for a in (
                        "first_names",
                        "last_name",
                        "title"
                    ):
                        data[a] = db_data[a]

                try:
                    costs['full'] += self.engine.execute(
                        tfr.select(whereclause=tfr.c.reg_num == rn)
                    ).fetchone()[1]
                except Exception:
                    pass
                try:
                    partial = self.engine.execute(
                        tpr.select(whereclause=tpr.c.reg_num == rn)
                    ).fetchone()
                    costs['banquet'] += partial["banquet_quantity"]
                    costs['convention'] += partial["convention_quantity"]
                    costs['theme'] += partial["theme_quantity"]
                except Exception:
                    pass

                try:
                    costs['pins'] += self.engine.execute(
                        tp.select(whereclause=tp.c.reg_num == rn)
                    ).fetchone()[1]
                except Exception:
                    pass

                data['initial_owed'] = Decimal(sum([COSTS[k] * v for (k,v) in costs.items()])).quantize(TWOPLACES)
                
                data['payments'] = []
                try:
                    res = self.engine.execute(
                        tpy.select(whereclause=tpy.c.reg_num == rn)
                    ).fetchall()
                    for r in res:
                        data['payments'].append(Payment(r.timestamp, Decimal(r.amount).quantize(TWOPLACES)))
                except Exception:
                    pass
                registrees.append(Registree(**data))
        return registrees

def build_payment_record(reg_num, out_dir):
    db = DB()
    registrees = db.get_registrees(reg_num)
    total_owed = 0
    reg_nums = [f'{r.reg_num:03}' for r in registrees]
    names = '_'.join([f"{r.first_names[0].lower()}_{r.last_name.lower().replace(' ', '_')}" for r in registrees])
    rn_fn = '_'.join(reg_nums)
    fn = f"mdc2020_payments_{rn_fn}_{names}.txt"
    if out_dir:
        fn = os.path.join(out_dir, fn)
    out = ['# MD410 Convention 2020 Payment Record {-}', ""]
    for r in registrees:
        out.extend(r.render())
        total_owed += r.owed
    out.extend(['', f'## TOTAL OWED: R{total_owed} {{-}}', ''])
    out.append(
        f"""\

Please make all payments to this account:

* **Bank**: Nedbank
* **Account Type**: Savings Account
* **Branch Code**: 138026
* **Account Number**: 2015836799
* **Account Name**: Convention 2020

Please make EFT payments rather than cash deposits wherever possible.

Use the reference "*MDC{'/'.join(reg_nums)}*" when making payments. 

Please send proof of payment for any payments made to [vanwykk@gmail.com](mailto:vanwykk@gmail.com) and [david.shone.za@gmail.com](mailto:david.shone.za@gmail.com).

Thank you again for registering for the 2020 MD410 Convention.
"""
    )                
    
    with open(fn, 'w') as fh:
        fh.write('\n'.join(out))
    return fn
                
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Build MD410 2020 Convention payment record markdown"
    )
    parser.add_argument("reg_num", type=int, help="The first reg_num to use.")
    parser.add_argument(
        "--out_dir", default="/io/", help="The directory to write output to."
    )
    args = parser.parse_args()
    
    print(build_payment_record(args.reg_num, args.out_dir))
