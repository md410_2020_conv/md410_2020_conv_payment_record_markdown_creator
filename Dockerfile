FROM registry.gitlab.com/kimvanwyk/python3-poetry-container:latest

COPY app/*.py /app/

ENTRYPOINT ["python", "/app/create_payment_record_markdown.py"]
